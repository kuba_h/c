#include <stdio.h>

int main(void)
{
    int month_1, day_1, year_1, month_2, day_2, year_2;

    printf("Enter a date (mm/dd/yy): ");
    scanf("%d/%d/%d", &month_1, &day_1, &year_1);

    printf("*** Enter 0/0/0 to end ***\n");
    while (1) {
        printf("Enter a date (mm/dd/yy): ");
        scanf("%d/%d/%d", &month_2, &day_2, &year_2);
        if ((year_2 == 0) && (month_2 == 0) && (day_2 == 0)) {
            printf("%d/%d/%.2d is the earliest date\n", month_1, day_1, year_1);
            break;
        } else if (year_2 < year_1) {
            year_1 = year_2;
            month_1 = month_2;
            day_1 = day_2;
        } else if ((year_2 == year_1) && (month_2 < month_1)) {
            year_1 = year_2;
            month_1 = month_2;
            day_1 = day_2;
        } else if ((year_2 == year_1) && (month_2 == month_1) && (day_2 < day_1)) {
            year_1 = year_2;
            month_1 = month_2;
            day_1 = day_2;
        }
    }

    return 0;
}
