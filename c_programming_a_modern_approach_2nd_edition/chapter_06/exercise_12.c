#include <stdio.h>

int main(void)
{
    int d = 0, n = 13;

    for (d = 2; (d * d) < n; d++)
        if (n % d == 0) {
            printf("%d is divisible by %d\n", n, d);
            return 0;
        }

    printf("%d is prime\n", n);

    return 0;
}
