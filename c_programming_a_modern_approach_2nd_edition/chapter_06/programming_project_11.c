#include <stdio.h>

int main(void)
{
    printf("*** Program approximates constant e ***\n");

    float e = 1.0f;
    int factorial = 1;

    int n;
    printf("Enter positive integer: ");
    scanf("%d", &n);

    for (int i = 1; i <= n; ++i) {
        factorial *= i;
        e += (1.0f / factorial);
    }

    printf("Value of e is %f\n", e);

    return 0;
}
