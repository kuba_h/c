/* Computing the GCD (Euclidean algorithm) */

#include <stdio.h>

int main(void)
{
    int m, n, remainder;

    printf("Enter two integers: ");
    scanf("%d %d", &m, &n);

    if ((m == 0) && (n == 0)) {
        printf("Wrong input!\n");
        return 0;
    }

    while (n > 0) {
        remainder = m % n;
        m = n;
        n = remainder;
    }

    printf("Greatest common divisor: %d\n", m);

    return 0;
}
