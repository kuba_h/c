#include <stdio.h>

int main(void)
{
    for (int i = 0; i <= 10; ++i) {
        if (i % 2 != 0)
            continue;
        printf("%d ", i);
    }
    printf("\n");

/* continue statement replaced by an equivalent goto statement */

    for (int i = 0; i <= 10; ++i) {
        if (i % 2 != 0)
            goto skip_loop;
        printf("%d ", i);
        skip_loop: ;
    }
    printf("\n");

    return 0;
}
