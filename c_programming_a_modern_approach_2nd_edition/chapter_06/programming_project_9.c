#include <stdio.h>

int main(void)
{
    float loan, interest, payment;
    int nr_of_payments;

    printf("Enter amount of loan: ");
    scanf("%f", &loan);
    printf("Enter interest rate: ");
    scanf("%f", &interest);
    printf("Enter monthly payment: ");
    scanf("%f", &payment);
    printf("Enter number of payments: ");
    scanf("%d", &nr_of_payments);

    for (int i = 0; i < nr_of_payments; ++i) {
        loan = loan - payment + (loan * ((interest / 12.0f) / 100.0f));
        printf("Remaining balance: %.2f\n", loan);
    }

    return 0;
}
