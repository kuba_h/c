#include <stdio.h>

int main(void)
{
    int number = 0;

    printf("Enter a number (n > 1): ");
    scanf("%d", &number);

    if (number <= 1) {
        printf("Wrong input!\n");
    }

    for (int i = 1, square = i * i; square <= number; ++i, square = i * i)
        if (square % 2 == 0)
            printf("%d\n", square);

    return 0;
}
