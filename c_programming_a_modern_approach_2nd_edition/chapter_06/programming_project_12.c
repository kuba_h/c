#include <stdio.h>

int main(void)
{
    float e = 1.0f;
    int factorial = 1;

    printf("*** Program approximates constant e ***\n");

    int n;
    printf("Enter positive integer: ");
    scanf("%d", &n);

    float epsilon;
    printf("The program continues adding terms until the current term ");
    printf("becomes less than epsilon\n");
    printf("Enter epsilon (small real number): ");
    scanf("%f", &epsilon);

    float term;

    for (int i = 1; i <= n; ++i) {
        factorial *= i;
        term = 1.0f / factorial;
        if (term >= epsilon)
            e += term;
        else
            break;
    }

    printf("Value of e is %f\n", e);

    return 0;
}
