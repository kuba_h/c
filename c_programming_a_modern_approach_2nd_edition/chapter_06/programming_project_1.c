#include <stdio.h>

int main(void)
{
    float largest_number = 0.0f;
    float number = 0.0f;

    printf("\n*** Program finds the largest number in a series of numbers ***\n");
    printf("*** End and display by entering 0 or negative number ***\n\n");

    for (;;) {
        printf("Enter a number: ");
        scanf("%f", &number);
        if (number <= 0)
            break;
        if (number > largest_number)
            largest_number = number;
    }

    if (largest_number > 0) {
        printf("The largest number entered was %f\n", largest_number);
        return 0;
    }

    if (number <= 0) {
        printf("The largest number entered was %f\n", number);
        return 0;
    }
}
