#include <stdio.h>

int main(void)
{
    int array_of_integers[5][5] = {0};
    int row_totals = 0, column_totals = 0;

    for (int i = 0; i < 5; ++i) {
        printf("Enter row %d: ", i + 1);
        for (int j = 0; j < 5; ++j)
            scanf("%d", &array_of_integers[i][j]);
    }

    printf("Row totals: ");
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j)
            row_totals += array_of_integers[i][j];
        printf("%d ", row_totals);
        row_totals = 0;
    }
    printf("\n");

    printf("Column totals: ");
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j)
            column_totals += array_of_integers[j][i];
        printf("%d ", column_totals);
        column_totals = 0;
    }
    printf("\n");

    return 0;
}
