/*
 * Chapter 8
 * Programming Project 15
 * Encrypts a message using a Caesar cipher
 */

#include <stdio.h>

int main(void)
{
    char message[80]; // array to store a message to encrypt
    char ch; // will read input into ch
    int index = 0; // counts number of chars entered
    int shift_amount = 0; // number of positions by which letters should be shifted

    printf("Enter message to be encrypted: ");
    while ((ch = getchar())) { // getting message into array
        if ((ch == '.') || (ch == '!') || (ch == '?')) { // loop termination condition
            message[index] = ch;
            ++index;
            break;
        }
        message[index] = ch;
        ++index;
    }

    printf("Enter shift amount (1-25): ");
    scanf("%d", &shift_amount);

    printf("Encrypted message: ");
    for (int i = 0; i < index; ++i) { // printing encrypted message
        if (message[i] >= 'a' && message[i] <= 'z') { // lower case letters
            message[i] = ((message[i] - 'a') + shift_amount) % 26 + 'a';
            putchar(message[i]);
        } else if (message[i] >= 'A' && message[i] <= 'Z') { // upper case letters
            message[i] = ((message[i] - 'A') + shift_amount) % 26 + 'A';
            putchar(message[i]);
        } else { // other chars
            putchar(message[i]);
        }
    }
    printf("\n");

    return 0;
}
