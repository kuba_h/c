#include <stdio.h>

int main(void)
{
    long n;

    printf("Enter a number: ");
    scanf("%ld", &n);
    while (n > 0) {
        int digit_seen[10] = {0};
        int digit;
        while (n > 0) {
            digit = n % 10;
            if (digit_seen[digit])
                digit_seen[digit] += 1;
            else
                digit_seen[digit] += 1;
            n /= 10;
        }
        printf("Digit:\t\t");
        for (int i = 0; i < 10; ++i)
            printf("%3d", i);
        printf("\n");
        printf("Occurrences:\t");
        for (int i = 0; i < 10; ++i)
            printf("%3d", digit_seen[i]);
        printf("\n");
        printf("Enter a number (n <= 0 to terminate): ");
        scanf("%ld", &n);
    }

    return 0;
}
