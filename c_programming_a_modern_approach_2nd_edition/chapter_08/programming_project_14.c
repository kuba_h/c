/*
 * Chapter 8 
 * Programming Project 14
 * Reverses the words in a sentence
 */

#include <stdio.h>

int main(void)
{
    char ch; // variable to store input chars one by one
    char sentence[80]; // array of char to store the sentence
    char terminating_char; // variable to store terminating character (. ! ?)
    int index = 0; // counting the number of entered chars

    printf("Enter a sentence: ");
    // loop to read char one by one and store it into array
    while ((ch = getchar())) {
        if ((ch == '.') || (ch == '!') || (ch == '?')) {
            terminating_char = ch;
            break;
        }
        sentence[index] = ch;
        ++index;
    }

    --index; // index lowered by 1 to stay within array boundaries
    int word = 0; // number of chars in each word in sentence
    int j = 0; // variable to help iterate in inner while loop
    int k = 0; // need to store current starting index of each word within sentence
    // loop going backwards over sentence printing words in reverse order
    while (index >= 0) {
        if (sentence[index] != ' ') // counting length of each word within sentence
            ++word;
        else { // else executes when space is encountered
            k = index + 1; // setting index to first char in word, adding 1 because index points to space
            while (j < word) { // looping over each word printing each char
                putchar(sentence[k]);
                ++j;
                ++k;
            }
            putchar(' '); // after printing each word space is printed after
            j = 0; // resetting all 3 counters after finishing with single word
            k = 0;
            word = 0;
        }
        --index; // decreasing index to move backwards within sentence array
    } // while loop ends but first word in sentence still needs to be printed
    k = index + 1; // setting k to point to first char of first word in sentence
    while (j < word) { // looping over first word
        putchar(sentence[k]);
        ++j;
        ++k;
    }
    putchar(terminating_char); // printing terminating char last
    printf("\n");

    return 0;
}
