/*
 * Chapter 8
 * Programming Project 16
 * Tests whether two words are anagrams
 */

#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int a_z_array[26] = {0}; // array to store letter occurrence
    char ch; // var to read input char by char

    printf("Enter first word: ");
    while ((ch = getchar()) != '\n') // reading first word
        if (isalpha(ch)) // skipping any non letters
            a_z_array[tolower(ch) - 'a'] += 1; // updating letter occurrence

    printf("Enter second word: ");
    while ((ch = getchar()) != '\n') // reading second word
        if (isalpha(ch))
            a_z_array[tolower(ch) - 'a'] -= 1; // decrementing each letter occurrence

    for (int i = 0; i < 26; ++i) // checking if all array elements are 0
        if (a_z_array[i] != 0) { // if not program ends
            printf("The words are not anagrams.\n");
            return 0;
        }
    printf("The words are anagrams.\n");

    return 0;
}
