#include <stdio.h>
#include <ctype.h>

int main(void)
{
    char message[30];
    char ch;
    int char_entered = 0;

    printf("Enter message: ");
    while ((ch = getchar()) != '\n') {
        message[char_entered] = ch;
        ++char_entered;
    }

    for (int i = 0; i < char_entered; ++i) {
        message[i] = toupper(message[i]);
        switch (message[i]) {
            case 'A':
                message[i] = '4';
                break;
            case 'B':
                message[i] = '8';
                break;
            case 'E':
                message[i] = '3';
                break;
            case 'I':
                message[i] = '1';
                break;
            case 'O':
                message[i] = '0';
                break;
            case 'S':
                message[i] = '5';
                break;
        }
    }

    printf("In B1FF-speak: ");
    for (int i = 0; i < char_entered; ++i)
        printf("%c", message[i]);
    for (int i = 0; i < 13; ++i)
        putchar('!');
    printf("\n");

    return 0;
}
