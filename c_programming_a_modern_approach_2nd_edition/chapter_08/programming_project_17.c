/*
 * Chapter 8
 * Programming Project 17
 * Prints magic square
 */

#include <stdio.h>

int main(void)
{
    printf("\nThis program creates a magic square of a specific size.\n");
    printf("The size must be an odd number between 1 and 99.\n");

    int size;
    printf("Enter size of magic square: ");
    scanf("%d", &size);

    int magic_square[size][size];
    // setting all values in array to 0
    for (int i = 0; i < size; ++i)
        for (int j = 0; j < size; ++j)
            magic_square[i][j] = 0;

    // stetting starting value of 1 in the magic array
    int row = 0;
    int col = size / 2;
    magic_square[row][col] = 1;
    // populating the array with values from 2 to size^2
    for (int i = 2; i <= size * size; ++i) {
        // checking if row, col are within array boundaries and value is 0
        if ((row - 1 >= 0 && col + 1 < size) && magic_square[row - 1][col + 1] == 0) {
            row -= 1; // adjusting row value
            col += 1; // adjusting col value
            magic_square[row][col] = i; // setting value to i
        // checking if row is within boundaries, col is outside and value after wrap around is 0
        } else if ((row - 1 >= 0 && col + 1 == size) && magic_square[row - 1][col - size + 1] == 0) {
            row -= 1;
            col = col - size + 1; // wrapping around because out of boundaries
            magic_square[row][col] = i;
        // row out of boundaries, col within and value is 0
        } else if ((row - 1 < 0 && col + 1 < size) && magic_square[row + size - 1][col + 1] == 0) {
            row = row + size - 1; // wrapping around
            col = col + 1;
            magic_square[row][col] = i;
        // row and col out of boundaries, value is 0
        } else if ((row - 1 < 0 && col + 1 == size) && magic_square[row + size - 1][col - size + 1] == 0) {
            row = row + size - 1; // wrapping
            col = col - size + 1; // wrapping
            magic_square[row][col] = i;
        // row and col within boundaries but value is non zero
        } else if ((row - 1 >= 0 && col + 1 < size) && magic_square[row - 1][col + 1] != 0) {
            row += 1; // going directly below current number
            magic_square[row][col] = i;
        // row within, col not, non zero value
        } else if ((row - 1 >= 0 && col + 1 == size) && magic_square[row - 1][col - size + 1] != 0) {
            row += 1; // going directly below current number
            magic_square[row][col] = i;
        // row out of boundaries, col within, non zero value
        } else if ((row - 1 < 0 && col + 1 < size) && magic_square[row + size - 1][col + 1] != 0) {
            row += 1;
            magic_square[row][col] = i;
        // row and col out of boundaries, non zero value
        } else if ((row - 1 < 0 && col + 1 == size) && magic_square[row + size - 1][col - size + 1] != 0) {
            row += 1;
            magic_square[row][col] = i;
        }
    }

    // prints magic square
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j)
            printf("%5d", magic_square[i][j]);
        printf("\n");
    }

    return 0;
}
