/*
 * Chapter 8
 * Exercise 9
 */

#include <stdio.h>

int main(void)
{
    float temperature_readings[30][24];

    for (int i = 0; i < 30; ++i)
        for (int j = 0; j < 24; ++j)
            temperature_readings[i][j] = i + j;

    float temp_sum = 0;
    for (int i = 0; i < 30; ++i)
        for (int j = 0; j < 24; ++j)
            temp_sum += temperature_readings[i][j];

    printf("Average temperature: %.2f\n", temp_sum / (30 * 24));

    return 0;
}
