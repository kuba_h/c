#include <stdio.h>

int main(void)
{
    float quiz_grades[5][5];
    float total_score = 0.0F, high_score, low_score;

    printf("\n");
    for (int i = 0; i < 5; ++i) {
        printf("Enter student %d grades: ", i + 1);
        for (int j = 0; j < 5; ++j)
            scanf("%f", &quiz_grades[i][j]);
    }
    printf("\n");

    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j)
            total_score += quiz_grades[i][j];
        printf("Total score for student %d: %.2f, average score: %.2f\n", i + 1, total_score, total_score / 5.0F);
        total_score = 0.0F;
    }
    printf("\n");

    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            total_score += quiz_grades[j][i];
            if (j == 0) {
                high_score = quiz_grades[j][i];
                low_score = quiz_grades[j][i];
            } else {
                if (quiz_grades[j][i] > high_score)
                    high_score = quiz_grades[j][i];
                else if (quiz_grades[j][i] < low_score)
                    low_score = quiz_grades[j][i];
            }
        }
        printf("Average for quiz %d: %.2f, high score: %.2f, low score: %.2f\n", i + 1, total_score / 5.0F, high_score, low_score);
        total_score = 0.0F;
    }
    printf("\n");

    return 0;
}
