#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    char char_arr[10][10] = {{'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'},
                             {'.','.','.','.','.','.','.','.','.','.'}};

    srand((unsigned) time(NULL));
    char start = 'A';
    int random;
    int row = 0, col = 0;
    char_arr[col][row] = start;

    while (start != 'Z') {
        random = rand() % 4;
        switch (random) {
            case 0:
                if ((row - 1 >= 0) && (char_arr[col][row - 1] == '.')) {
                    row -= 1;
                    char_arr[col][row] = ++start;
                } else if ((row + 1 < 10) && (char_arr[col][row + 1] == '.')) {
                    row += 1;
                    char_arr[col][row] = ++start;
                } else if ((col - 1 >= 0) && (char_arr[col - 1][row] == '.')) {
                    col -= 1;
                    char_arr[col][row] = ++start;
                } else if ((col + 1 < 10) && (char_arr[col + 1][row] == '.')) {
                    col += 1;
                    char_arr[col][row] = ++start;
                } else {
                    goto while_end;
                }
                break;
            case 1:
                if ((row + 1 < 10) && (char_arr[col][row + 1] == '.')) {
                    row += 1;
                    char_arr[col][row] = ++start;
                } else if ((row - 1 >= 0) && (char_arr[col][row - 1] == '.')) {
                    row -= 1;
                    char_arr[col][row] = ++start;
                } else if ((col - 1 >= 0) && (char_arr[col - 1][row] == '.')) {
                    col -= 1;
                    char_arr[col][row] = ++start;
                } else if ((col + 1 < 10) && (char_arr[col + 1][row] == '.')) {
                    col += 1;
                    char_arr[col][row] = ++start;
                } else {
                    goto while_end;
                }
                break;
            case 2:
                if ((col - 1 >= 0) && (char_arr[col - 1][row] == '.')) {
                    col -= 1;
                    char_arr[col][row] = ++start;
                } else if ((row + 1 < 10) && (char_arr[col][row + 1] == '.')) {
                    row += 1;
                    char_arr[col][row] = ++start;
                } else if ((row - 1 >= 0) && (char_arr[col][row - 1] == '.')) {
                    row -= 1;
                    char_arr[col][row] = ++start;
                } else if ((col + 1 < 10) && (char_arr[col + 1][row] == '.')) {
                    col += 1;
                    char_arr[col][row] = ++start;
                } else {
                    goto while_end;
                }
                break;
            case 3:
                if ((col + 1 < 10) && (char_arr[col + 1][row] == '.')) {
                    col += 1;
                    char_arr[col][row] = ++start;
                } else if ((row + 1 < 10) && (char_arr[col][row + 1] == '.')) {
                    row += 1;
                    char_arr[col][row] = ++start;
                } else if ((col - 1 >= 0) && (char_arr[col - 1][row] == '.')) {
                    col -= 1;
                    char_arr[col][row] = ++start;
                } else if ((row - 1 >= 0) && (char_arr[col][row - 1] == '.')) {
                    row -= 1;
                    char_arr[col][row] = ++start;
                }else {
                    goto while_end;
                }
                break;
        }
    }
    while_end:;

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j)
            printf("%c ", char_arr[i][j]);
        printf("\n");
    }

    return 0;
}
