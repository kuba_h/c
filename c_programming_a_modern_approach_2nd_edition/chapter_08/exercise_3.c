/*
 * Chapter 8
 * Exercise 3
 */

#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    bool weekend[7] = {true, false, false, false, false, false, true};
    printf("%d%d%d%d%d%d%d\n", weekend[0], weekend[1], weekend[2], weekend[3],
        weekend[4], weekend[5], weekend[6]);

    return 0;
}
