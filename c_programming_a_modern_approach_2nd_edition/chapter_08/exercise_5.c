/*
 * Chapter 8
 * Exercise 5
 */

#include <stdio.h>

int main(void)
{
    int fib_numbers[40] = {[1] = 1};
    for (int i = 2; i < 40; ++i)
        fib_numbers[i] = fib_numbers[i - 2] + fib_numbers[i - 1];

    for (int i = 0; i < 40; ++i)
        printf("%3d%10d\n",i, fib_numbers[i]);

return 0;
}
