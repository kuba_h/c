#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int departure_times[8] = {480, 583, 679, 767, 840, 945, 1140, 1305};
    int arrival_times[8] = {616, 712, 811, 900, 968, 1075, 1280, 1438};
    int hour, minute, minutes_since_midnight;

    printf("Enter a 24-hour time: ");
    scanf("%d:%d", &hour, &minute);
    minutes_since_midnight = (hour * 60) + minute;

    int time_d = abs(minutes_since_midnight - departure_times[0]);
    int index = 0;

    for (int i = 1; i < 8; ++i)
        if (abs(minutes_since_midnight - departure_times[i]) < time_d) {
            time_d = abs(minutes_since_midnight - departure_times[i]);
            index = i;
        }

    printf("Closest departure time is %d:%.2d, arriving at %d:%.2d\n",
                    departure_times[index] / 60, departure_times[index] % 60,
                    arrival_times[index] / 60, arrival_times[index] % 60);

    return 0;
}
