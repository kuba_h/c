#include <stdio.h>

int main(void)
{
    char last_name[20];
    char ch_name, ch_last_name;
    int index = 1;

    printf("Enter a first and last name: ");
    scanf(" %1c", &ch_name);

    while ((ch_last_name = getchar()))
        if ((ch_last_name == ' ') || (ch_last_name == '\t'))
            break;

    scanf(" %1c", &last_name[0]);
    while (((ch_last_name = getchar()) != '\n')) {
        if ((ch_last_name != ' ') && (ch_last_name != '\t')) {
            last_name[index] = ch_last_name;
            ++index;
        }
    }

    printf("You entered the name: ");
    for (int i = 0; i < index; ++i)
        putchar(last_name[i]);
    printf(", ");
    putchar(ch_name);
    printf(".\n");

    return 0;
}
