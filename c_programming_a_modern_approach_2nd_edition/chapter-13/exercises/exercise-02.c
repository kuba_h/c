#include <stdio.h>

int main(int argc, char *argv[])
{
    char *p = "abc";

    //a
    //putchar(p); //incorrect
    //b
    putchar(*p);
    //c
    puts(p);
    //d
    //puts(*p); //incorrect

    return 0;
}
