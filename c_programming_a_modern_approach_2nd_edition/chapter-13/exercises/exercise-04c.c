#include <stdio.h>
#include <ctype.h>
#include <string.h>

int read_line(char str[], int n);

int main(int argc, char *argv[])
{
    char str[11];
    read_line(str, 10);
    printf("%s %d", str, strlen(str));
    return 0;
}

int read_line(char str[], int n)
{
    int ch, i = 0;
    do {
        ch = getchar();
        if (i < n)
            str[i++] = ch;
    } while (ch != '\n');
    str[i] = '\0';          /* terminates string */
    return i;               /* number of characters stored */
}
