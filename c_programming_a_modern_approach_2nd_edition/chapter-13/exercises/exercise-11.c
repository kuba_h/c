#include <stdio.h>

int strcmp(char *s, char *t);

int main(int argc, char *argv[])
{
    char *s = "abc";
    char *t = "def";

    printf("%d\n", strcmp(s, t));

    return 0;
}

int strcmp(char *s, char *t)
{
    int i;

    for (i = 0; *(s + i) == *(t + i); i++)
        if (*(s + i) == '\0')
            return 0;
    return *(s + i) - *(t + i);
}
