#include <stdio.h>

void censor(char str[]);

int main(int argc, char *argv[])
{
    char str[] = "food fool f";
    censor(str);
    printf("%s\n", str);

    return 0;
}

void censor(char str[])
{
    while (*str) {
        if (*str == 'f' && *(str + 1) == 'o' && *(str + 2) == 'o') {
            *str = 'x';
            *(str + 1) = 'x';
            *(str + 2) = 'x';
        }
        ++str;
    }
}
