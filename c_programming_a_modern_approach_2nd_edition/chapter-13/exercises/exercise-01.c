#include <stdio.h>

int main(int argc, char *argv[])
{
    //a
    printf("%c", '\n');
    //b
    //printf("%c", "\n"); //incorrect
    //c
    //printf("%s", '\n'); //incorrect
    //d
    printf("%s", "\n");
    //e
    //printf('\n'); //incorrect
    //f
    printf("\n");
    //g
    putchar('\n');
    //h
    //putchar("\n"); //incorrect
    //i
    //puts('\n'); //incorrect
    //j
    puts("\n");
    //k
    puts("");

    return 0;
}
