#include <stdio.h>
#include <ctype.h>

int read_line(char str[], int n);

int main(int argc, char *argv[])
{
    char str[11];
    read_line(str, 10);
    printf("%s\n", str);
    return 0;
}

int read_line(char str[], int n)
{
    int ch, i = 0;
    while ((ch = getchar()) != '\n') {
        if (isspace(ch) && i == 0)
            continue;
        if (i < n)
            str[i++] = ch;
    }
    str[i] = '\0';          /* terminates string */
    return i;               /* number of characters stored */
}
