#include <stdio.h>
#include <string.h>

char *duplicate(const char *p, char *q);

int main(int argc, char *argv[])
{
    char s[] = "abc", q[5];
    duplicate(s, q);
    printf("%s\n", s);
    printf("%s\n", q);

    return 0;
}

char *duplicate(const char *p, char *q)
{
    strcpy(q, p);
    return q;
}
