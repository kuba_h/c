#include <stdio.h>
#include <ctype.h>

int read_line(char str[], int n);

int main(int argc, char *argv[])
{
    char str[11];
    char str2[11] = {'\0'};
    read_line(str, 10);
    scanf("%s", str2);
    printf("%s\n", str);
    printf("%s\n", str2);
    return 0;
}

int read_line(char str[], int n)
{
    int ch, i = 0;
    while ((ch = getchar()) != '\n') {
        if (i < n)
            str[i++] = ch;
        if (i == n)
            break;
    }
    str[i] = '\0';          /* terminates string */
    return i;               /* number of characters stored */
}
