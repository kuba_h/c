#include <stdio.h>
#include <ctype.h>

void capitalize(char str[]);

int main(int argc, char *argv[])
{
    char str[] = "kuba123";
    capitalize(str);
    printf("%s\n", str);

    return 0;
}

void capitalize(char str[])
{
    int index = 0;
    while (str[index]) {
        if (isalpha(str[index])) {
            str[index] = toupper(str[index]);
        }
        ++index;
    }
}
