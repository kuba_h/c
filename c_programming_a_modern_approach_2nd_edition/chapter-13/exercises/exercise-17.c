#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

bool test_extension(const char *file_name, const char *extension);

int main(int argc, char *argv[])
{
    printf("%d\n", test_extension("memo.txt", "TXT"));
    printf("%d\n", test_extension("memo.txt", "JPEG"));
    printf("%d\n", test_extension("memo.doc", "DOC"));
    printf("%d\n", test_extension("memo.doc", "TXT"));
    return 0;
}

bool test_extension(const char *file_name, const char *extension)
{
    while (*file_name)
        file_name++;

    int extension_len = strlen(extension);
    file_name -= extension_len;
    for (int i = 0; i < extension_len; i++) {
        if (!(toupper(*file_name++) == *extension++))
            return false;
    }
    return true;
}
