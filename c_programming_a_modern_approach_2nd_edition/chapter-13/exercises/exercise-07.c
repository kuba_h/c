#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
    char str[] = "abc";

    //a
    //*str = 0;
    //printf("%s\n", str);

    //b
    //str[0] = '\0';
    //printf("%s\n", str);

    //c
    //strcpy(str, "");
    //printf("%s\n", str);

    //d
    strcat(str, "");
    printf("%s\n", str);

    return 0;
}
