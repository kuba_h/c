#include <stdio.h>
#include <string.h>

void get_extension(const char *file_name, char *extension);

int main(int argc, char *argv[])
{
    char *fn = "memo.txt";
    char extension[5];
    get_extension(fn, extension);
    printf("%s\n", extension);

    return 0;
}

void get_extension(const char *file_name, char *extension)
{
    for (int i = 0; i < strlen(file_name); i++)
        if (file_name[i] == '.') {
            strcpy(extension, file_name + i + 1);
            return;
        }
    strcpy(extension, "");
}
