#include <stdio.h>

int count_spaces(const char *s);

int main(int argc, char *argv[])
{
    char *s = "a b c d e";
    printf("%d\n", count_spaces(s));
    return 0;
}

int count_spaces(const char *s)
{
    int count = 0;
    while (*s) {
        if (*s == ' ')
            count++;
        s++;
    }
    return count;
}
