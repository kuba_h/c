#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
    char smallest_word[21] = {'\0'};
    char largest_word[21] = {'\0'};
    char temp_word[21] = {'\0'};

    printf("Enter word: ");
    scanf("%s", temp_word);
    strcpy(smallest_word, temp_word);
    strcpy(largest_word, temp_word);
    while (strlen(temp_word) != 4) {
        printf("Enter word: ");
        scanf("%s", temp_word);
        if (strcmp(temp_word, smallest_word) < 0)
            strcpy(smallest_word, temp_word);
        else if (strcmp(temp_word, largest_word) > 0)
            strcpy(largest_word, temp_word);
    }

    printf("\nSmallest word: %s\n", smallest_word);
    printf("Largest word: %s\n\n", largest_word);

    return 0;
}
