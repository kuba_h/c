/*
 * Chapter 11
 * Exercise 1
 */

#include <stdio.h>

int main(void)
{
    int i = 13, *p = &i;

    //a
    *p = 42;
    printf("%d\n", i);
    //b
    printf("%p\n", &p);
    //c
    int *q = &i;
    *&p = q;
    printf("%d\n", *q);
    **&p = 17;
    printf("%d\n", i);
    //d
    printf("%p\n", &*p);
    printf("%p\n", &i);
    *&*p = 23;
    printf("%d\n", i);
    //e
    //*i;
    //f
    printf("%p\n", &i);
    //g
    *&i = 123;
    printf("%d\n", i);
    //h
    //&*i;

    return 0;
}
