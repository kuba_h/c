/*
 * Chapter 11
 * Exercise 7
 */

#include <stdio.h>

void split_date(int day_of_year, int year, int *month, int *day);

int main(void)
{
    int day_of_year = 366, year = 2000, month, day;

    split_date(day_of_year, year, &month, &day);

    printf("Month: %d, day: %d\n", month, day);

    return 0;
}

void split_date(int day_of_year, int year, int *month, int *day)
{
    int days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    if (year % 4 == 0)
        if (year % 100 != 0)
            days_in_month[1] = 29;
        else if (year % 100 == 0 && year % 400 == 0)
            days_in_month[1] = 29;

    *month = 1;
    if (days_in_month[0] >= day_of_year) {
        *day = day_of_year;
        return;
    }

    int sum = days_in_month[0];
    for (int i = 1; i < 12; ++i) {
        if (sum + days_in_month[i] < day_of_year) {
            sum += days_in_month[i];
            *month += 1;
        } else if (sum + days_in_month[i] == day_of_year) {
            *month += 1;
            *day = days_in_month[i];
            return;
        } else {
            *month += 1;
            *day = day_of_year - sum;
            return;
        }
    }
}
