/*
 * Chapter 11
 * Exercise 8
 */

#include <stdio.h>

int *find_largest(int a[], int n);

int main(void)
{
    int a[] = {0, 1, 2, 3, 4, 13, 5, 6, 7, 8}, n = 10, *p;

    p = find_largest(a, n);

    printf("Largest element: %d\n", *p);

    return 0;
}

int *find_largest(int a[], int n)
{
    int largest = a[0];
    int index = 0;
    for (int i = 1; i < n; ++i)
        if (a[i] > largest) {
            largest = a[i];
            index = i;
        }

    return &a[index];
}
