/*
 * Chapter 11
 * Programming Project 3
 */

#include <stdio.h>

void reduce(int numerator, int denominator, int *reduced_numerator, int *reduced_denominator);

int main(void)
{
    int numerator, denominator, reduced_numerator, reduced_denominator;

    printf("Enter a fraction: ");
    scanf("%d/%d", &numerator, &denominator);

    if ((numerator == 0) || (denominator == 0)) {
        printf("Wrong input!\n");
        return 0;
    }

    reduce(numerator, denominator, &reduced_numerator, &reduced_denominator);

    printf("In lowest terms: %d/%d\n", reduced_numerator, reduced_denominator);

    return 0;
}

void reduce(int numerator, int denominator, int *reduced_numerator, int *reduced_denominator)
{
    *reduced_numerator = numerator;
    *reduced_denominator = denominator;

    int remainder;
    while (denominator > 0) {
        remainder = numerator % denominator;
        numerator = denominator;
        denominator = remainder;
    }

    *reduced_numerator /= numerator;
    *reduced_denominator /= numerator;
}
