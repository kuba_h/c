/*
 * Chapter 11
 * Exercise 6
 */

#include <stdio.h>

void find_two_largest(int a[], int n, int *largest, int *second_largest);

int main(void)
{
    int a[] = {2, 3, 5, 7, 11, 13, 0}, n = 7, largest, second_largest;

    find_two_largest(a, n, &largest, &second_largest);

    printf("Largest: %d\n", largest);
    printf("Second largest: %d\n", second_largest);

    return 0;
}

void find_two_largest(int a[], int n, int *largest, int *second_largest)
{
    *largest = a[0];
    for (int i = 1; i < n; ++i)
        if (a[i] > *largest)
            *largest = a[i];

    *second_largest = a[0];
    for (int i = 1; i < n; ++i)
        if (a[i] > *second_largest && a[i] != *largest)
            *second_largest = a[i];
}
