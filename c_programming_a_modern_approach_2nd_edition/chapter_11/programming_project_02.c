/*
 * Chapter 11
 * Programming Project 2
 */

#include <stdio.h>

void find_closest_flight(int desired_time, int *departure_time, int *arrival_time);

int main(void)
{
    int hour, minute, minutes_since_midnight, departure_time, arrival_time;

    printf("Enter a 24-hour time: ");
    scanf("%d:%d", &hour, &minute);

    minutes_since_midnight = (hour * 60) + minute;

    find_closest_flight(minutes_since_midnight, &departure_time, &arrival_time);

    printf("Closest departure time is ");
    printf("%d", (departure_time >= 780) ? departure_time / 60 - 12 : departure_time / 60);
    printf(":");
    printf("%.2d %s,", departure_time % 60, (departure_time <= 719) ? "a.m." : "p.m.");
    printf(" arriving at ");
    printf("%d", (arrival_time >= 780) ? arrival_time / 60 - 12 : arrival_time / 60);
    printf(":");
    printf("%.2d %s\n", arrival_time % 60, (arrival_time <= 719) ? "a.m." : "p.m.");

    return 0;
}

void find_closest_flight(int desired_time, int *departure_time, int *arrival_time)
{
    if (desired_time <= 532) {
        *departure_time = 480;
        *arrival_time = 616;
    }
    else if (desired_time <= 631) {
        *departure_time = 583;
        *arrival_time = 712;
    }
    else if (desired_time <= 723) {
        *departure_time = 679;
        *arrival_time = 811;
    }
    else if (desired_time <= 804) {
        *departure_time = 767;
        *arrival_time = 900;
    }
    else if (desired_time <= 893) {
        *departure_time = 840;
        *arrival_time = 968;
    }
    else if (desired_time <= 1043) {
        *departure_time = 945;
        *arrival_time = 1075;
    }
    else if (desired_time <= 1223) {
        *departure_time = 1140;
        *arrival_time = 1280;
    }
    else {
        *departure_time = 1305;
        *arrival_time = 1438;
    }
}
