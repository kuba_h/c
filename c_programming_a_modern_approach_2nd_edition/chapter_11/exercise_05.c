/*
 * Chapter 11
 * Exercise 5
 */

#include <stdio.h>

void split_time(long total_sec, int *hr, int *min, int *sec);

int main(void)
{
    long total_sec = 23 * 60 * 60 + 59 * 60 + 59;
    int hr, min, sec;

    split_time(total_sec, &hr, &min, &sec);

    printf("%d %d %d\n", hr, min, sec);

    return 0;
}

void split_time(long total_sec, int *hr, int *min, int *sec)
{
    *hr = (total_sec / 60) / 60;
    total_sec -= *hr * 60 * 60;
    *min = total_sec / 60;
    total_sec -= *min * 60;
    *sec = total_sec;
}
