/*
 * Chapter 11
 * Exercise 4
 */

#include <stdio.h>

void swap(int *p, int *q);

int main(void)
{
    int p = 13, q = 17;

    printf("%d %d\n", p, q);
    swap(&p, &q);
    printf("%d %d\n", p, q);

    return 0;
}

void swap(int *p, int *q)
{
    int temp = *p;
    *p = *q;
    *q = temp;
}
