/*
 * Chapter 11
 * Exercise 3
 */

#include <stdio.h>

void avg_sum(double a[], int n, double *avg, double *sum);

int main(int argc, char const *argv[])
{
    int n = 10;
    double sum, avg, a[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    avg_sum(a, n, &avg, &sum);

    printf("Sum: %f\n", sum);
    printf("Average: %f\n", avg);

    return 0;
}

void avg_sum(double a[], int n, double *avg, double *sum)
{
    int i;

    *sum = 0.0;
    for (i = 0; i < n; i++)
        *sum += a[i];
    *avg = *sum / n;
}
