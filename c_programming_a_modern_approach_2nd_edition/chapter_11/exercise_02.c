/*
 * Chapter 11
 * Exercise 2
 */

int main(void)
{
    int i = 3, *p, *q;

    //a
    //p = i;
    //b
    //*p = &i;
    //c
    //&p = q;
    //d
    //p = &q;
    //e
    p = *&q;
    //f
    p = q;
    //g
    //p = *q;
    //h
    //*p = q;
    //i
    *p = *q;

    return 0;
}
