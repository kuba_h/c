/*
 * Chapter 11
 * Programming Project 1
 */

#include <stdio.h>

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones);

int main(void)
{
    int dollars, twenties, tens, fives, ones;
    printf("Enter a U.S. dollar amount: ");
    scanf("%d", &dollars);

    pay_amount(dollars, &twenties, &tens, &fives, &ones);

    printf("$20 dollar bills: %d\n", twenties);
    printf("$10 dollar bills: %d\n", tens);
    printf(" $5 dollar bills: %d\n", fives);
    printf(" $1 dollar bills: %d\n", ones);

    return 0;
}

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones)
{
    *twenties = dollars / 20;
    dollars = dollars - 20 * *twenties;
    *tens = dollars / 10;
    dollars = dollars - 10 * *tens;
    *fives = dollars / 5;
    dollars = dollars - 5 * *fives;
    *ones = dollars;
}
