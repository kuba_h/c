/*
 * Chapter 9
 * Programming Project 1
 * Sorts the array of integers using selection sort recursively
 */

#include <stdio.h>

void selection_sort(int a[], int n);

int main(void)
{
    int arr[10];

    printf("Enter a series of 10 integers: ");
    for (int i = 0; i < 10; ++i)
        scanf("%d", &arr[i]);

    selection_sort(arr, 10);
    printf("Sorted array: ");
    for (int i = 0; i < 10; ++i)
        printf("%d ", arr[i]);
    printf("\n");

    return 0;
}

void selection_sort(int a[], int n)
{
    if (n == 1)
        return;

    int largest = a[0];
    int index = 0;

    for (int i = 1; i < n; ++i) {
        if (a[i] > largest) {
            largest = a[i];
            index = i;
        }
    }

    int temp = a[n - 1];
    a[n - 1] = largest;
    a[index] = temp;

    selection_sort(a, n - 1);
}
