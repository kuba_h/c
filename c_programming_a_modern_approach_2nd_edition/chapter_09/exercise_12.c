/*
 * Chapter 9
 * Exercise 12
 */

#include <stdio.h>

double inner_product(double a[], double b[], int n);

int main(void)
{
    double a[3] = {1.22, 2.33, 3.44};
    double b[3] = {9.88, 8.77, 7.66};

    printf("Inner product: %f\n", inner_product(a, b, 3));

    return 0;
}

double inner_product(double a[], double b[], int n)
{
    double inner_product = a[0] * b[0];

    for (int i = 1; i < n; ++i)
        inner_product += a[i] * b[i];

    return inner_product;
}
