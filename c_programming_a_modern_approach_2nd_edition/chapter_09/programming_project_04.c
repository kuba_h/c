/*
 * Chapter 9
 * Programming Project 4
 * Tests whether two words are anagrams
 */

#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

void read_word(int counts[26]);
bool equal_array(int counts1[26], int counts2[26]);

int main(void)
{
    int a_z_array1[26] = {0};
    int a_z_array2[26] = {0};

    printf("Enter first word: ");
    read_word(a_z_array1);
    printf("Enter second word: ");
    read_word(a_z_array2);

    if (equal_array(a_z_array1, a_z_array2))
            printf("The words are anagrams.\n");
    else
        printf("The words are not anagrams.\n");

    return 0;
}

void read_word(int counts[26])
{
    char ch;
    while ((ch = getchar()) != '\n')
        if (isalpha(ch))
            counts[tolower(ch) - 'a'] += 1;
}

bool equal_array(int counts1[26], int counts2[26])
{
    for (int i = 0; i < 26; ++i)
        if (counts1[i] != counts2[i])
            return false;
    return true;
}
