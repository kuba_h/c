/*
 * Chapter 9
 * Exercise 7
 */

#include <stdio.h>

int f(int a, int b);

int main(void)
{
    int i;
    double x;

    // a
    i = f(83, 12);
    printf("%d\n", i);

    // b
    x = f(83, 12);
    printf("%f\n", x);

    // c
    i = f(3.15, 9.28);
    printf("%d\n", i);

    // d
    x = f(3.15, 9.28);
    printf("%f\n", x);

    // e
    f(83, 12);

    return 0;
}

int f(int a, int b)
{
    return a + b;
}
