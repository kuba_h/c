/*
 * Chapter 9
 * Programming Project 2
 */

#include <stdio.h>

float tax_due(float income);

int main(void)
{
    float income;
    printf("Enter the amount of taxable income: ");
    scanf("%f", &income);

    printf("Tax due: $%.2f\n", tax_due(income));

    return 0;
}

float tax_due(float income)
{
    float tax_due;

    if (income < 750)
        tax_due = income * 0.01f;
    else if (income < 2250)
        tax_due = 7.5f + ((income - 750) * 0.02f);
    else if (income < 3750)
        tax_due = 37.5f + ((income - 2250) * 0.03f);
    else if (income < 5250)
        tax_due = 82.5f + ((income - 3750) * 0.04f);
    else if (income < 7000)
        tax_due = 142.5f + ((income - 5250) * 0.05f);
    else
        tax_due = 230.0f + ((income - 7000) * 0.06f);

    return tax_due;
}
