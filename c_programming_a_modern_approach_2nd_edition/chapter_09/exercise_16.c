/*
 * Chapter 9
 * Exercise 16
 */

#include <stdio.h>

int fact(int n);

int main(void)
{
    printf("Factorial of 10 = %d\n", fact(10));

    return 0;
}

int fact(int n)
{
    return n <= 1 ? 1 : n * fact(n - 1);
}
