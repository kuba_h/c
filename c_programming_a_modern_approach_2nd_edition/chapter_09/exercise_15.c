/*
 * Chapter 9
 * Exercise 15
 */

#include <stdio.h>

double median(double x, double y, double z);

int main(void)
{
    double x = 1.0, y = 2.0, z = 3.0;

    printf("Median of three numbers: %f\n", median(x, y, z));

    return 0;
}

double median(double x, double y, double z)
{
    double median = 0.0;

    if (x >= y)
        if (y >= z)
            median = y;
        else if (x >= z)
            median = z;
        else
            median = x;
    else
        if (x >= z)
            median = x;
        else if (y >= z)
            median = z;
        else
            median = y;

    return median;
}
