/*
 * Chapter 9
 * Exercise 4
 */

#include <stdio.h>

int day_of_year(int month, int day, int year);

int main(void)
{
    int month, day, year;

    printf("Enter date (mm/dd/yyyy): ");
    scanf("%d/%d/%d", &month, &day, &year);

    printf("Day of year: %d\n", day_of_year(month, day, year));

    return 0;
}

int day_of_year(int month, int day, int year)
{
    int year_common[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int year_leap[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int day_of_year = 0;
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
        for (int i = 0; i < month - 1; ++i)
            day_of_year += year_leap[i];
    else
        for (int i = 0; i < month - 1; ++i)
            day_of_year += year_common[i];

    return day_of_year + day;
}
