/*
 * Chapter 9
 * Exercise 10
 */

#include <stdio.h>

int largest_element(int a[], int n);
float average(int a[], int n);
int positive_elements(int a[], int n);

int main(void)
{
    int arr[5] = {-2, -1, 0, 1, 2};

    printf("Largest element: %d\n", largest_element(arr, 5));
    printf("Average of all elements: %f\n", average(arr, 5));
    printf("Number of positive elements: %d\n", positive_elements(arr, 5));

    return 0;
}

int largest_element(int a[], int n)
{
    int largest = a[0];
    for (int i = 1; i < n; ++i)
        if (a[i] > largest)
            largest = a[i];
    return largest;
}

float average(int a[], int n)
{
    int sum = 0;
    for (int i = 0; i < n; ++i)
        sum += a[i];
    return (float) sum / (float) n;
}

int positive_elements(int a[], int n)
{
    int positive = 0;
    for (int i = 0; i < n; ++i)
        if (a[i] > 0)
            ++positive;
    return positive;
}
