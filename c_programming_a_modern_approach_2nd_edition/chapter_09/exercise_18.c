/*
 * Chapter 9
 * Exercise 18
 */

#include <stdio.h>

int gcd(int m, int n);

int main(void)
{
    int m, n;

    printf("Enter m: ");
    scanf("%d", &m);
    printf("Enter n: ");
    scanf("%d", &n);

    printf("Greatest common divisor: %d\n", gcd(m, n));

    return 0;
}

int gcd(int m, int n)
{
    if (n == 0)
        return m;

    return gcd(n, m % n);
}
