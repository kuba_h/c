/*
 * Chapter 9
 * Programming Project 8
 * Simulates the game of craps
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

int roll_dice(void);
bool play_game(void);

int main(void)
{
    printf("*** Game of Craps ***\n");

    srand((unsigned) time(NULL));
    int number_of_wins = 0, number_of_losses = 0;
    char c = 'y';

    while (c == 'y' || c == 'Y') {
        bool play = play_game();
        if (play) {
            printf("You win!\n\n");
            ++number_of_wins;
        } else {
            printf("You lose!\n\n");
            ++number_of_losses;
        }
        printf("Play again? ");
        scanf(" %c", &c);
        while (getchar() != '\n');
        printf("\n");
    }

    printf("Wins: %d Losses: %d\n\n", number_of_wins, number_of_losses);

    return 0;
}

int roll_dice(void)
{
    return (((rand() % 6) + 1) + ((rand() % 6) + 1));
}

bool play_game(void)
{
    int first_roll = roll_dice();
    if (first_roll == 7 || first_roll == 11) {
        printf("You rolled: %d\n", first_roll);
        return true;
    } else if (first_roll == 2 || first_roll == 3 || first_roll == 12) {
        printf("You rolled: %d\n", first_roll);
        return false;
    } else {
        printf("You rolled: %d\n", first_roll);
        printf("Your point is: %d\n", first_roll);
        int next_roll = roll_dice();
        while (1) {
            printf("You rolled: %d\n", next_roll);
            if (next_roll == first_roll)
                return true;
            else if (next_roll == 7)
                return false;
            else
                next_roll = roll_dice();
        }
    }
}
