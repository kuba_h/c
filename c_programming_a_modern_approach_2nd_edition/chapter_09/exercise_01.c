/*
 * Chapter 9
 * Exercise 1
 */

#include <stdio.h>

double triangle_area(double base, double height);

int main(void)
{
    double base, height;

    printf("Enter base: ");
    scanf("%lf", &base);
    printf("Enter height: ");
    scanf("%lf", &height);

    printf("Triangle area: %.2f\n", triangle_area(base, height));

    return 0;
}

double triangle_area(double base, double height)
{
    double product;
    product = base * height;
    return product / 2;
}
