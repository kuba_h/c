/*
 * Chapter 9
 * Exercise 6
 */

#include <stdio.h>

int digit(int n, int k);

int main(void)
{
    int n, k;

    printf("Returns the k-th digit (from the right) in a positive integer.\n");
    printf("Enter a positive integer: ");
    scanf("%d", &n);
    printf("Enter k: ");
    scanf("%d", &k);

    printf("Digit: %d\n", digit(n, k));

    return 0;
}

int digit(int n, int k)
{
    int number = n;
    int number_of_digits = 0;
    int digit;

    do {
        ++number_of_digits;
        n /= 10;
    } while (n != 0);

    if (k > number_of_digits)
        return 0;
    else {
        do {
            digit = number % 10;
            number /= 10;
            --k;
        } while (k > 0);
        return digit;
    }
}
