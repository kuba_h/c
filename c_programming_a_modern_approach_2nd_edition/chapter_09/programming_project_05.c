/*
 * Chapter 9
 * Programming Project 5
 */

#include <stdio.h>

void create_magic_square(int n, int magic_square[n][n]);
void print_magic_square(int n, int magic_square[n][n]);

int main(void)
{
    printf("\nThis program creates a magic square of a specific size.\n");
    printf("The size must be an odd number between 1 and 99.\n");

    int size;
    printf("Enter size of magic square: ");
    scanf("%d", &size);

    int magic_square[size][size];

    create_magic_square(size, magic_square);
    print_magic_square(size, magic_square);

    return 0;
}

void create_magic_square(int n, int magic_square[n][n])
{
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            magic_square[i][j] = 0;

    int row = 0;
    int col = n / 2;
    magic_square[row][col] = 1;
    for (int i = 2; i <= n * n; ++i) {
        // checking if row, col are within array boundaries and value is 0
        if ((row - 1 >= 0 && col + 1 < n) && magic_square[row - 1][col + 1] == 0) {
            row -= 1;
            col += 1;
            magic_square[row][col] = i;
        // checking if row is within boundaries, col is outside and value after wrap around is 0
        } else if ((row - 1 >= 0 && col + 1 == n) && magic_square[row - 1][col - n + 1] == 0) {
            row -= 1;
            col = col - n + 1; // wrapping around because out of boundaries
            magic_square[row][col] = i;
        // row out of boundaries, col within and value is 0
        } else if ((row - 1 < 0 && col + 1 < n) && magic_square[row + n - 1][col + 1] == 0) {
            row = row + n - 1; // wrapping around
            col = col + 1;
            magic_square[row][col] = i;
        // row and col out of boundaries, value is 0
        } else if ((row - 1 < 0 && col + 1 == n) && magic_square[row + n - 1][col - n + 1] == 0) {
            row = row + n - 1; // wrapping
            col = col - n + 1; // wrapping
            magic_square[row][col] = i;
        // row and col within boundaries but value is non zero
        } else if ((row - 1 >= 0 && col + 1 < n) && magic_square[row - 1][col + 1] != 0) {
            row += 1; // going directly below current number
            magic_square[row][col] = i;
        // row within, col not, non zero value
        } else if ((row - 1 >= 0 && col + 1 == n) && magic_square[row - 1][col - n + 1] != 0) {
            row += 1; // going directly below current number
            magic_square[row][col] = i;
        // row out of boundaries, col within, non zero value
        } else if ((row - 1 < 0 && col + 1 < n) && magic_square[row + n - 1][col + 1] != 0) {
            row += 1;
            magic_square[row][col] = i;
        // row and col out of boundaries, non zero value
        } else if ((row - 1 < 0 && col + 1 == n) && magic_square[row + n - 1][col - n + 1] != 0) {
            row += 1;
            magic_square[row][col] = i;
        }
    }
}

void print_magic_square(int n, int magic_square[n][n])
{
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j)
            printf("%5d", magic_square[i][j]);
        printf("\n");
    }
}
