/*
 * Chapter 9
 * Programming Project 3
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void generate_random_walk(char walk[10][10]);
void print_array(char walk[10][10]);

int main(void)
{
    char char_arr[10][10];

    generate_random_walk(char_arr);
    print_array(char_arr);

    return 0;
}

void generate_random_walk(char walk[10][10])
{
    for (int i = 0; i < 10; ++i)
        for (int j = 0; j < 10; ++j)
            walk[i][j] = '.';

    srand((unsigned) time(NULL));
    char start = 'A';
    int random;
    int row = 0, col = 0;
    walk[col][row] = start;

    while (start != 'Z') {
        random = rand() % 4;
        switch (random) {
            case 0:
                if ((row - 1 >= 0) && (walk[col][row - 1] == '.')) {
                    row -= 1;
                    walk[col][row] = ++start;
                } else if ((row + 1 < 10) && (walk[col][row + 1] == '.')) {
                    row += 1;
                    walk[col][row] = ++start;
                } else if ((col - 1 >= 0) && (walk[col - 1][row] == '.')) {
                    col -= 1;
                    walk[col][row] = ++start;
                } else if ((col + 1 < 10) && (walk[col + 1][row] == '.')) {
                    col += 1;
                    walk[col][row] = ++start;
                } else {
                    goto while_end;
                }
                break;
            case 1:
                if ((row + 1 < 10) && (walk[col][row + 1] == '.')) {
                    row += 1;
                    walk[col][row] = ++start;
                } else if ((row - 1 >= 0) && (walk[col][row - 1] == '.')) {
                    row -= 1;
                    walk[col][row] = ++start;
                } else if ((col - 1 >= 0) && (walk[col - 1][row] == '.')) {
                    col -= 1;
                    walk[col][row] = ++start;
                } else if ((col + 1 < 10) && (walk[col + 1][row] == '.')) {
                    col += 1;
                    walk[col][row] = ++start;
                } else {
                    goto while_end;
                }
                break;
            case 2:
                if ((col - 1 >= 0) && (walk[col - 1][row] == '.')) {
                    col -= 1;
                    walk[col][row] = ++start;
                } else if ((row + 1 < 10) && (walk[col][row + 1] == '.')) {
                    row += 1;
                    walk[col][row] = ++start;
                } else if ((row - 1 >= 0) && (walk[col][row - 1] == '.')) {
                    row -= 1;
                    walk[col][row] = ++start;
                } else if ((col + 1 < 10) && (walk[col + 1][row] == '.')) {
                    col += 1;
                    walk[col][row] = ++start;
                } else {
                    goto while_end;
                }
                break;
            case 3:
                if ((col + 1 < 10) && (walk[col + 1][row] == '.')) {
                    col += 1;
                    walk[col][row] = ++start;
                } else if ((row + 1 < 10) && (walk[col][row + 1] == '.')) {
                    row += 1;
                    walk[col][row] = ++start;
                } else if ((col - 1 >= 0) && (walk[col - 1][row] == '.')) {
                    col -= 1;
                    walk[col][row] = ++start;
                } else if ((row - 1 >= 0) && (walk[col][row - 1] == '.')) {
                    row -= 1;
                    walk[col][row] = ++start;
                }else {
                    goto while_end;
                }
                break;
        }
    }
    while_end:;
}

void print_array(char walk[10][10])
{
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j)
            printf("%c ", walk[i][j]);
        printf("\n");
    }
}
