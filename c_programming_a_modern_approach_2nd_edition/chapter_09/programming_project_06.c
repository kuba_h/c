/*
 * Chapter 9
 * Programming Project 6
 */

#include <stdio.h>

float polynomial(float x);

int main(void)
{
    float x;
    printf("Enter a value for x: ");
    scanf("%f", &x);

    printf("Value of the polynomial: %f\n", polynomial(x));

    return 0;
}

float polynomial(float x)
{
    return ((((3 * x + 2) * x - 5) * x - 1) * x + 7) * x - 6;
}
