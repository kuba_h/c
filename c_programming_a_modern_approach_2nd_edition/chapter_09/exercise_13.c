/*
 * Chapter 9
 * Exercise 13
 */

#include <stdio.h>

int evaluate_position(char board[8][8]);

int main(void)
{
    char board[8][8] = {[0][0] = 'k', [0][1] = 'q', [7][0] = 'K', [7][1] = 'Q'};

    printf("Chess position evaluation: %d\n", evaluate_position(board));

    return 0;
}

int evaluate_position(char board[8][8])
{
    int evaluate_position_white = 0;
    int evaluate_position_black = 0;

    for (int i = 0; i < 8; ++i)
        for (int j = 0; j < 8; ++j)
            switch (board[i][j]) {
                case 'Q':
                    evaluate_position_white += 9; break;
                case 'R':
                    evaluate_position_white += 5; break;
                case 'B':
                    evaluate_position_white += 3; break;
                case 'N':
                    evaluate_position_white += 3; break;
                case 'P':
                    evaluate_position_white += 1; break;
                case 'q':
                    evaluate_position_black += 9; break;
                case 'r':
                    evaluate_position_black += 5; break;
                case 'b':
                    evaluate_position_black += 3; break;
                case 'n':
                    evaluate_position_black += 3; break;
                case 'p':
                    evaluate_position_black += 1; break;
            }

    return evaluate_position_white - evaluate_position_black;
}
