/*
 * Chapter 9
 * Exercise 11
 */

#include <stdio.h>
#include <ctype.h>

float compute_GPA(char grades[], int n);

int main(void)
{
    char grades[5] = {'A', 'b', 'C', 'd', 'F'};

    printf("Average of the grades: %.2f\n", compute_GPA(grades, 5));

    return 0;
}

float compute_GPA(char grades[], int n)
{
    int sum = 0;
    for (int i = 0; i < n; ++i)
        switch (toupper(grades[i])) {
            case 'A':
                sum += 4; break;
            case 'B':
                sum += 3; break;
            case 'C':
                sum += 2; break;
            case 'D':
                sum += 1; break;
        }
    return (float) sum / (float) n;
}
