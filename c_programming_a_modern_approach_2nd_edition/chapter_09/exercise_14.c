/*
 * Chapter 9
 * Exercise 14
 */

#include <stdio.h>
#include <stdbool.h>

bool has_zero(int a[], int n);

int main(void)
{
    int arr[3] = {2, 1, 0};

    printf("%d\n", has_zero(arr, 3));

    return 0;
}

bool has_zero(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++)
        if (a[i] == 0)
            return true;

    return false;
}
