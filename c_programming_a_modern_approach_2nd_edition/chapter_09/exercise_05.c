/*
 * Chapter 9
 * Exercise 5
 */

#include <stdio.h>

int num_digits(int n);

int main(void)
{
    int number;

    printf("Enter a positive integer: ");
    scanf("%d", &number);

    printf("Number of digits: %d\n", num_digits(number));

    return 0;
}

int num_digits(int n)
{
    int number_of_digits = 0;

    do {
        ++number_of_digits;
        n /= 10;
    } while (n != 0);

    return number_of_digits;
}
