/*
 * Chapter 7
 * Programming Project 6
 */

#include <stdio.h>

int main(void)
{
    printf("\vSize of int: %zu\n", sizeof(int));
    printf("Size of short: %zu\n", sizeof(short));
    printf("Size of long: %zu\n", sizeof(long));
    printf("Size of float: %zu\n", sizeof(float));
    printf("Size of double: %zu\n", sizeof(double));
    printf("Size of long double: %zu\n\v", sizeof(long double));

    return 0;
}
