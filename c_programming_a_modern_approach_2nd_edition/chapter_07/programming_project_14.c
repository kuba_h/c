/*
 * Chapter 7
 * Programming Project 14
 */

#include <stdio.h>
#include <math.h>

int main(void)
{
    double x, y = 1.0, x_y, average_of_y_and_x_y;

    printf("Enter a positive number: ");
    scanf("%lf", &x);

    if (x <= 0) {
        printf("Wrong input!\n");
        return 0;
    }

    do {
        x_y = x / y;
        average_of_y_and_x_y = (y + x_y) / 2.0;
        if (fabs(y - average_of_y_and_x_y) < (0.00001 * y))
            break;
        y = average_of_y_and_x_y;
    } while (1);

    printf("Square root: %f\n", y);

    return 0;
}
