/*
 * Chapter 7
 * Programming Project 1
 */

#include <stdio.h>

int main(void)
{
    //short i, n; // failure for n = 182, 16-bits to store short
    //int i, n; // failure for n = 46340, 32-bits to store int
    long i, n; // works for n = 10000000, 64-bits to store long

    printf("This program prints a table of squares.\n");
    printf("Enter number of entries in table: ");
    //scanf("%hd", &n);
    //scanf("%d", &n);
    scanf("%ld", &n);

    for (i = 1; i <= n; i++)
        //printf("%10hd%10hd\n", i, i * i);
        //printf("%10d%20d\n", i, i * i);
        printf("%10ld%30ld\n", i, i * i);

    return 0;
}
