/*
 * Chapter 7
 * Programming Project 13
 */

#include <stdio.h>

int main(void)
{
    int white_space = 0, letters = 0;
    char ch;

    printf("Enter a sentence: ");
    do {
        ch = getchar();
        if ((ch != '\n') && (ch != ' '))
            ++letters;
        else if ((ch == ' ') || (ch == '\n'))
            ++white_space;
    } while (ch != '\n');
    printf("Average word length: %.1f\n", (float)letters / white_space);

    return 0;
}
