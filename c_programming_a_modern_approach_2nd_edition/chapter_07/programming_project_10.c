/*
 * Chapter 7
 * Programming Project 10
 */

#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int vowels = 0;
    char ch;

    printf("Enter a sentence: ");
    while ((ch = toupper(getchar())) != '\n')
        switch (ch) {
            case 'A': case 'E': case 'I':
            case 'O': case 'U':
                ++vowels;
                break;
        }

    printf("Your sentence contains %d vowels.\n", vowels);

    return 0;
}
