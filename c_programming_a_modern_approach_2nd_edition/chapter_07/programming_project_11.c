/*
 * Chapter 7
 * Programming Project 11
 */

#include <stdio.h>

int main(void)
{
    char ch_name, ch_last_name;

    printf("Enter a first and last name: ");
    scanf(" %1c", &ch_name);

    while ((ch_last_name = getchar()))
        if ((ch_last_name == ' ') || (ch_last_name == '\t'))
            break;

    scanf(" %1c", &ch_last_name);
    while (ch_last_name != '\n') {
        if ((ch_last_name != ' ') && (ch_last_name != '\t')) {
            putchar(ch_last_name);
        }
        ch_last_name = getchar();
    }

    printf(", ");
    putchar(ch_name);
    printf(".\n");

    return 0;
}
