/*
 * Chapter 7
 * Programming Project 3
 */

#include <stdio.h>

int main(void)
{
    double n, sum = 0.0;

    printf("\vThis program sums a series of real numbers.\n");
    printf("Enter real numbers (0 to terminate): ");

    scanf("%lf", &n);
    while (n != 0) {
        sum += n;
        scanf("%lf", &n);
    }
    printf("The sum is: %f\n\v", sum);

    return 0;
}
