/*
 * Chapter 7
 * Programming Project 15
 */

#include <stdio.h>

int main(void)
{
    //short factorial = 1; /* largest correct value for n = 7 */
    //int factorial = 1; /* largest correct value for n = 12 */
    //long factorial = 1; /* largest correct value for n = 20 */
    //long long factorial = 1; // largest correct value for n = 20 */
    //float factorial = 1.0F; // largest value for n = 34 */
    //double factorial = 1.0; // largest value for n = 34 */
    long double factorial = 1.0L; // largest value for n = 1754 */

    int n;
    printf("Enter a positive integer: ");
    scanf("%d", &n);

    if (n < 0) {
        printf("Wrong input!\n");
        return 0;
    }

    for (int i = 1; i <= n; ++i)
        factorial *= i;

    printf("Factorial of %d: %Lf\n", n, factorial);

    return 0;
}
