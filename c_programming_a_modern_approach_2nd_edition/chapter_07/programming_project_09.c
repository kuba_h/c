/*
 * Chapter 7
 * Programming Project 9
 */

#include <stdio.h>

int main(void)
{
    int hour, minute;
    char ch;

    printf("Enter a 12-hour time: ");
    scanf("%d:%d %c", &hour, &minute, &ch);

    if ((ch == 'P') || (ch == 'p'))
        hour = hour + 12;

    printf("Equivalent 24-hour time: %d:%.2d\n", hour, minute);

    return 0;
}
