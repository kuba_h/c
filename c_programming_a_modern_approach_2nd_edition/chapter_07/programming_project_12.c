/*
 * Chapter 7
 * Programming Project 12
 */

#include <stdio.h>

int main(void)
{
    float value_of_expression, operand;
    char ch;

    printf("Enter an expression: ");
    scanf("%f", &value_of_expression);
    do {
        scanf("%c", &ch);
        if (ch == '\n')
            break;
        scanf("%f", &operand);
        switch (ch) {
            case '+':
                value_of_expression += operand;
                break;
            case '-':
                value_of_expression -= operand;
                break;
            case '*':
                value_of_expression *= operand;
                break;
            case '/':
                value_of_expression /= operand;
                break;
        }
    } while (1);
    printf("Value of expression: %.1f\n", value_of_expression);

    return 0;
}
