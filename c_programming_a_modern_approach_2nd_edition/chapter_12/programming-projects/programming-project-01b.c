/*
 * Chapter 12
 * Programming Project 1
 * Programming Project 3
 */

#include <stdio.h>

#define N 100

int main(void)
{
    char c, c_array[N], *pc = c_array;
    
    printf("\nEnter a message: ");
    while (((c = getchar()) != '\n') && (pc < c_array + N)) {
        *pc++ = c;     
    }
    
    printf("\nReversal is: ");
    for (char *pce = pc - 1; pce >= c_array; --pce) {
        putchar(*pce);
    }
    printf("\n\n");

    return 0;
}

