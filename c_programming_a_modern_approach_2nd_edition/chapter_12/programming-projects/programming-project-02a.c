/*
 * Chapter 12
 * Programming Project 2
 */

#include <stdio.h>
#include <ctype.h>

#define N 100

int main(void)
{
    char c, c_array[N];
    int count = 0;
    
    printf("\nEnter a message: ");
    while (((c = getchar()) != '\n') && (count <= N)) {
        c_array[count] = c;
        ++count;
    }
    
    for (int i = 0, j = count - 1; i < j;) {
        if (!isalpha(c_array[i])) ++i;
        if (!isalpha(c_array[j])) --j;
        if (isalpha(c_array[i]) && isalpha(c_array[j])) {
            if (tolower(c_array[i]) == tolower(c_array[j])) {
                ++i;
                --j;
            } else {
                printf("Not a palindrome\n\n");
                return 0;
            }
        }
    }
    printf("Palindrome\n\n");

    return 0;
}

