/*
 * Chapter 12
 * Programming Project 2
 * Programming Project 4
 */

#include <stdio.h>
#include <ctype.h>

#define N 100

int main(void)
{
    char c, c_array[N], *pc = c_array;
    
    printf("\nEnter a message: ");
    while (((c = getchar()) != '\n') && (pc < c_array + N)) {
        *pc++ = c;
    }
    
    for (char *pcb = c_array, *pce = pc - 1; pcb < pce;) {
        if (!isalpha(*pcb)) ++pcb;
        if (!isalpha(*pce)) --pce;
        if (isalpha(*pcb) && isalpha(*pce)) {
            if (tolower(*pcb) == tolower(*pce)) {
                ++pcb;
                --pce;
            } else {
                printf("Not a palindrome\n\n");
                return 0;
            }
        }
    }
    printf("Palindrome\n\n");

    return 0;
}

