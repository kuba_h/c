/*
 * Chapter 12
 * Programming Project 1
 */

#include <stdio.h>

#define N 100

int main(void)
{
    char c, c_array[N];
    int count = 0;
    
    printf("\nEnter a message: ");
    while (((c = getchar()) != '\n') && (count <= N)) {
        c_array[count] = c;
        ++count;
    }
    
    printf("\nReversal is: ");
    for (int i = count - 1; i >= 0; --i) {
        putchar(c_array[i]);
    }
    printf("\n\n");

    return 0;
}

