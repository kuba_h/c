/*
 * Chapter 12
 * Exercise 14
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

bool search(const int a[], int n, int key);

int main(void)
{
    int temperatures[7][24];

    srand((unsigned) time(NULL));
    for (int *p = temperatures[0]; p <= &temperatures[6][23]; p++)
        *p = (rand() % 40) - 5;

    for (int *p = temperatures[0]; p <= &temperatures[6][23]; p++)
        printf("%d ", *p);

    printf("\n%d\n", search(temperatures[0], 7 * 24, 32));

    return 0;
}

bool search(const int a[], int n, int key)
{
    for (const int *p = a; p < a + n; p++)
        if (*p == key)
            return true;
    return false;
}
