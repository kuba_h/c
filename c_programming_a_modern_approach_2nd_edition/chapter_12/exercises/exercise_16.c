/*
 * Chapter 12
 * Exercise 16
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int find_largest(int a[], int n);

int main(void)
{
    int temperatures[7][24];

    srand((unsigned) time(NULL));
    for (int *p = temperatures[0]; p <= &temperatures[6][23]; p++)
        *p = (rand() % 40) - 5;

    for (int i = 0; i < 7; ++i)
        printf("%d\n", find_largest(temperatures[i], 24));

    return 0;
}

int find_largest(int a[], int n)
{
    int *p = a, max;

    max = *p++;
    for (; p < a + n; p++)
        if (*p > max)
            max = *p;
    return max;
}
