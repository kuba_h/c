/*
 * Chapter 12
 * Exercise 7
 */

#include <stdio.h>
#include <stdbool.h>

bool search(const int a[], int n, int key);

int main(void)
{
    int a[] = {1, 2, 3, 4, 5}, n = 5, key = 0;

    printf("%d\n", search(a, n, key));

    return 0;
}

bool search(const int a[], int n, int key)
{
    for (const int *p = a; p < a + n; p++)
        if (*p == key)
            return true;
    return false;
}
