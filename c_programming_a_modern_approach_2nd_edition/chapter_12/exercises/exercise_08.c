/*
 * Chapter 12
 * Exercise 8
 */

#include <stdio.h>

void store_zeros(int a[], int n);

int main(void)
{
    int a[] = {1, 2, 3};

    for (int i = 0; i < 3; ++i)
        printf("%d ", a[i]);
    printf("\n");

    store_zeros(a, 3);

    for (int i = 0; i < 3; ++i)
        printf("%d ", a[i]);
    printf("\n");

    return 0;
}

void store_zeros(int a[], int n)
{
    int *p;

    for (p = a; p < a + n; p++)
        *p = 0;
}
