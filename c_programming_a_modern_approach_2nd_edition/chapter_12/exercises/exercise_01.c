/*
 * Chapter 12
 * Exercise 1
 */

#include <stdio.h>

int main(void)
{
    int a[] = {5, 15, 34, 54, 14, 2, 52, 72};
    int *p = &a[1], *q = &a[5];

    //a
    printf("%d\n", *(p + 3));
    //b
    printf("%d\n", *(q - 3));
    //c
    printf("%d\n", q - p);
    //d
    printf("%d\n", p < q);
    //e
    printf("%d\n", *p < *q);

    return 0;
}
