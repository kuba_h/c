/*
 * Chapter 12
 * Exercise 15
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    int temperatures[7][24], i;

    srand((unsigned) time(NULL));
    for (int *p = temperatures[0]; p <= &temperatures[6][23]; p++)
        *p = (rand() % 40) - 5;

    i = 3;
    for (int *p = temperatures[i]; p <= temperatures[i] + 23; p++)
        printf("%d ", *p);
    printf("\n");

    return 0;
}
