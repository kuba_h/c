/*
 * Chapter 12
 * Exercise 5
 */

#include <stdio.h>

int main(void)
{
    int a[] = {1, 2, 3}, *p;
    p = a;

    //printf("(a) %d\n", p == a[0]);

    printf("(b) %d\n", p == &a[0]);

    printf("(c) %d\n", *p == a[0]);

    printf("(d) %d\n", p[0] == a[0]);

    return 0;
}
