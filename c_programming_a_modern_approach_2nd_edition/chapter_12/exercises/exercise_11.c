/*
 * Chapter 12
 * Exercise 11
 */

#include <stdio.h>

int find_largest(int a[], int n);

int main(void)
{
    int a[] = {3, 7, 1, 4, 5};

    printf("%d\n", find_largest(a, 5));

    return 0;
}

int find_largest(int a[], int n)
{
    int *p = a, max;

    max = *p++;
    for (; p < a + n; p++)
        if (*p > max)
            max = *p;
    return max;
}
