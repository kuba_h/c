/*
 * Chapter 12
 * Exercise 17
 */

#include <stdio.h>

#define LEN 3

int sum_two_dimensional_array(const int a[][LEN], int n);

int main(void)
{
    int a[2][LEN] = {1, 2, 3, 4, 5, 6};

    printf("%d\n", sum_two_dimensional_array(a, 2));

    return 0;
}

int sum_two_dimensional_array(const int a[][LEN], int n)
{
    int sum = 0;

    for (const int *p = a[0]; p <= &a[n - 1][LEN - 1]; p++)
        sum += *p;

    return sum;
}
