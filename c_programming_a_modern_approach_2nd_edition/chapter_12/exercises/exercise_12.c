/*
 * Chapter 12
 * Exercise 12
 */

#include <stdio.h>

void find_two_largest(const int *a, int n, int *largest, int *second_largest);

int main(void)
{
    int a[] = {3, 4, 2, 6, 8, 5, 1, 7, 9, 0}, largest, second_largest;

    find_two_largest(a, 10, &largest, &second_largest);

    printf("%d %d\n", largest, second_largest);

    return 0;
}

void find_two_largest(const int *a, int n, int *largest, int *second_largest)
{
    const int *p = a;

    *largest = *p++;
    *second_largest = *p++;

    if (*largest < *second_largest) {
        int temp = *largest;
        *largest = *second_largest;
        *second_largest = temp;
    }

    for (; p < a + n; p++)
        if (*p > *largest) {
            *second_largest = *largest;
            *largest = *p;
        } else if (*p > *second_largest) {
            *second_largest = *p;
        }
}
