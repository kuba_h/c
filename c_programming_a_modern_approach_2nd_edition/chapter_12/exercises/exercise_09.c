/*
 * Chapter 12
 * Exercise 9
 */

#include <stdio.h>

double inner_product(const double *a, const double *b, int n);

int main(void)
{
    double a[] = {1, 2, 3}, b[] = {4, 5, 6};
    int n = 3;

    printf("%f\n", inner_product(a, b, n));

    return 0;
}

double inner_product(const double *a, const double *b, int n)
{
    double inner_product = 0.0;

    for (const double *p = a, *q = b; p < a + n; p++, q++)
        inner_product += *p * *q;

    return inner_product;
}
