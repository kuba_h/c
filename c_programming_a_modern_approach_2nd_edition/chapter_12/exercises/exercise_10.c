/*
 * Chapter 12
 * Exercise 10
 */

#include <stdio.h>

int *find_middle(int a[], int n);

int main(void)
{
    int a[] = {1, 2, 3};

    printf("%d\n", *(find_middle(a, 3)));

    return 0;
}

int *find_middle(int a[], int n)
{
    return a + n / 2;
}
