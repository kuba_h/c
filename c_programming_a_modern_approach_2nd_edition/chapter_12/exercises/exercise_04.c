/*
 * Chapter 12
 * Exercise 4
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define STACK_SIZE 100

int contents[STACK_SIZE];
int *top_ptr = &contents[0];

void make_empty(void);
bool is_empty(void);
bool is_full(void);
void stack_overflow(void);
void stack_underflow(void);
void push(int i);
int pop(void);

int main(void)
{
    printf("%d\n", is_empty());
    printf("%d\n", is_full());

    push(6);
    printf("%d\n", is_empty());
    printf("%d\n", is_full());

    push(7);
    printf("%d\n", is_empty());
    printf("%d\n", is_full());

    printf("%d\n", pop());
    printf("%d\n", pop());
    printf("%d\n", is_empty());
    printf("%d\n", is_full());

    return 0;
}

void make_empty(void)
{
    top_ptr = &contents[0];
}

bool is_empty(void)
{
    return top_ptr == &contents[0];
}

bool is_full(void)
{
    return top_ptr == &contents[STACK_SIZE];
}

void stack_overflow(void)
{
    exit(EXIT_FAILURE);
}

void stack_underflow(void)
{
    exit(EXIT_FAILURE);
}

void push(int i)
{
    if (is_full())
        stack_overflow();
    else
        *top_ptr++ = i;
}

int pop(void)
{
    if (is_empty())
        stack_underflow();
    else
        return *--top_ptr;
}
