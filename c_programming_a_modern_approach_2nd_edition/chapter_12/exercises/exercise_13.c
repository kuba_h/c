/*
 * Chapter 12
 * Exercise 13
 */

#include <stdio.h>

#define N 5

int main(void)
{
    double ident[N][N], *p = &ident[0][0];
    int count = 0;
    *p++ = 1.0;

    for (; p <= &ident[N - 1][N - 1]; p++)
        if (count < N) {
            *p = 0.0;
            ++count;
        } else if (count == N) {
            *p = 1.0;
            count = 0;
        }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j)
            printf("%f ", ident[i][j]);
        printf("\n");
    }

    return 0;
}
