#include <stdio.h>

int main(void)
{
    int i, j, k, l;
    int largest_1, smallest_1, largest_2, smallest_2;

    printf("Enter four integers: ");
    scanf("%d %d %d %d", &i, &j, &k, &l);

    if (i > j) {
        largest_1 = i;
        smallest_1 = j;
    } else {
        largest_1 = j;
        smallest_1 = i;
    }

    if (k > l) {
        largest_2 = k;
        smallest_2 = l;
    } else {
        largest_2 = l;
        smallest_2 = k;
    }

    if (largest_1 > largest_2)
        printf("Largest: %d\n", largest_1);
    else
        printf("Largest: %d\n", largest_2);

    if (smallest_1 < smallest_2)
        printf("Smallest: %d\n", smallest_1);
    else
        printf("Smallest: %d\n", smallest_2);

    return 0;
}
