#include <stdio.h>

int main(void)
{
    int hour, minute;

    printf("Enter a 24-hour time: ");
    scanf("%d:%d", &hour, &minute);

    printf("Equivalent 12-hour time: %d:%.2d ", hour > 12 ? hour - 12 : hour, minute);
    printf("%s\n", (hour > 11 && hour < 24) ? "PM" : "AM");

    return 0;
}
