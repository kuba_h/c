/*
 * Chapter 10
 * Programming Project 1
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#define STACK_SIZE 100

/* external variables */
char contents[STACK_SIZE];
int top = 0;

/* prototypes */
void make_empty(void);
bool is_empty();
bool is_full();
void push(char c);
char pop(void);
void stack_overflow(void);
void stack_underflow(void);

int main(void)
{
    char c;
    printf("Enter parentheses and/or braces: ");
    while ((c = getchar()) != '\n') {
        if (c == '(' || c == '{')
            push(c);
        else if (c == ')' || c == '}') {
            char c_pop = pop();
            if ((c_pop == '(' && c == ')') || (c_pop == '{' && c == '}'))
                continue;
            else
                break;
        }
    }

    if (c == '\n' && is_empty())
        printf("Parentheses/braces are nested properly\n");
    else
        printf("Parentheses/braces are not nested properly\n");

    return 0;
}

void make_empty(void)
{
    top = 0;
}

bool is_empty(void)
{
    return top == 0;
}

bool is_full(void)
{
    return top == STACK_SIZE;
}

void push(char c)
{
    if (is_full()) {
        stack_overflow();
    }
    else
        contents[top++] = c;
}

char pop(void)
{
    if (is_empty()) {
        stack_underflow();
    }
    else
        return contents[--top];
}

void stack_overflow(void)
{
    printf("Stack overflow\n");
    exit(EXIT_FAILURE);
}

void stack_underflow(void)
{
    printf("Stack underflow\n");
    exit(EXIT_FAILURE);
}
