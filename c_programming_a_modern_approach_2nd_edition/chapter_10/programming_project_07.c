/*
 * Chapter 10
 * Programming Project 7
 * Prompts the user for a number and then displays the number,
 * using characters to simulate the effect of a seven-segment display.
 */

#include <stdio.h>
#include <stdbool.h>

#define MAX_DIGITS 10

const bool segments[10][7] = {{1, 1, 1, 1, 1, 1, 0}, // 0
                              {0, 1, 1, 0, 0, 0, 0}, // 1
                              {1, 1, 0, 1, 1, 0, 1}, // 2
                              {1, 1, 1, 1, 0, 0, 1}, // 3
                              {0, 1, 1, 0, 0, 1, 1}, // 4
                              {1, 0, 1, 1, 0, 1, 1}, // 5
                              {1, 0, 1, 1, 1, 1, 1}, // 6
                              {1, 1, 1, 0, 0, 0, 0}, // 7
                              {1, 1, 1, 1, 1, 1, 1}, // 8
                              {1, 1, 1, 1, 0, 1, 1}};// 9
char digits[4][MAX_DIGITS * 4];

void clear_digits_array(void);
void process_digit(int digit, int position);
void print_digits_array(void);

int main(void)
{
    clear_digits_array();

    char c;
    int position = 0;
    printf("Enter a number: ");
    while (((c = getchar()) != '\n') && (position < MAX_DIGITS * 4)) {
        if (c >= '0' && c <= '9') {
            process_digit(c - '0', position);
            position += 4;
        }
    }

    print_digits_array();

    return 0;
}

void clear_digits_array(void)
{
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < MAX_DIGITS * 4; ++j)
            digits[i][j] = ' ';
}

void process_digit(int digit, int position)
{
    if (segments[digit][0]) digits[0][position + 1] = '_';
    if (segments[digit][1]) digits[1][position + 2] = '|';
    if (segments[digit][2]) digits[2][position + 2] = '|';
    if (segments[digit][3]) digits[2][position + 1] = '_';
    if (segments[digit][4]) digits[2][position] = '|';
    if (segments[digit][5]) digits[1][position] = '|';
    if (segments[digit][6]) digits[1][position + 1] = '_';
}

void print_digits_array(void)
{
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < MAX_DIGITS * 4; ++j)
            printf("%c", digits[i][j]);
        printf("\n");
    }
}
