#include <stdio.h>

int main(void)
{
    float volume = (4.0f / 3.0f) * 3.1416f * 10 * 10 * 10;
    printf("Volume of a sphere with a 10-meter radius: %f\n", volume);

    return 0;
}
