#include <stdio.h>

int main(void)
{
    float radius;
    printf("Enter the radius of the sphere: ");
    scanf("%f", &radius);

    float volume = (4.0f / 3.0f) * 3.1416f * radius * radius * radius;
    printf("Volume of a sphere with a %.2f-meter radius: %f\n", radius, volume);

    return 0;
}
