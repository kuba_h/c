#include <stdio.h>

int main(void)
{
    int amount;
    printf("Enter a U.S. dollar amount: ");
    scanf("%d", &amount);

    printf("$20 dollar bills: %d\n", amount / 20);
    amount = amount - 20 * (amount / 20);
    printf("$10 dollar bills: %d\n", amount / 10);
    amount = amount - 10 * (amount / 10);
    printf(" $5 dollar bills: %d\n", amount / 5);
    amount = amount - 5 * (amount / 5);
    printf(" $1 dollar bills: %d\n", amount / 1);

    return 0;
}
