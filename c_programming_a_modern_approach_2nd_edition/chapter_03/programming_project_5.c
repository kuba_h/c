#include <stdio.h>

int main(void)
{
    int a1, a2, a3, a4;
    int b1, b2, b3, b4;
    int c1, c2, c3, c4;
    int d1, d2, d3, d4;

    printf("Enter the numbers from 1 to 16 in any order:\n");
    scanf("%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", \
        &a1, &a2, &a3, &a4, &b1, &b2, &b3, &b4, \
        &c1, &c2, &c3, &c4, &d1, &d2, &d3, &d4);

    printf("\n");

    printf("%2d %2d %2d %2d\n", a1, a2, a3, a4);
    printf("%2d %2d %2d %2d\n", b1, b2, b3, b4);
    printf("%2d %2d %2d %2d\n", c1, c2, c3, c4);
    printf("%2d %2d %2d %2d\n", d1, d2, d3, d4);

    printf("\n");

    int sum_row_a = a1 + a2 + a3 + a4;
    int sum_row_b = b1 + b2 + b3 + b4;
    int sum_row_c = c1 + c2 + c3 + c4;
    int sum_row_d = d1 + d2 + d3 + d4;

    printf("Row sums: %d %d %d %d\n", sum_row_a, sum_row_b, \
        sum_row_c, sum_row_d);

    int sum_col_a1 = a1 + b1 + c1 + d1;
    int sum_col_a2 = a2 + b2 + c2 + d2;
    int sum_col_a3 = a3 + b3 + c3 + d3;
    int sum_col_a4 = a4 + b4 + c4 + d4;

    printf("Column sums: %d %d %d %d\n", sum_col_a1, sum_col_a2, \
        sum_col_a3, sum_col_a4);

    int sum_diag_a1 = a1 + b2 + c3 + d4;
    int sum_diag_d1 = d1 + c2 + b3 + a4;

    printf("Diagonal sums: %d %d\n", sum_diag_a1, sum_diag_d1);

    return 0;
}
