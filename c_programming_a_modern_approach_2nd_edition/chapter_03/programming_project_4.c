#include <stdio.h>

int main(void)
{
    int phone_nr_part_1, phone_nr_part_2, phone_nr_part_3;

    printf("Enter phone number [(xxx) xxx-xxxx]: ");
    scanf("(%d)%d-%d", &phone_nr_part_1, &phone_nr_part_2, &phone_nr_part_3);

    printf("You entered %.3d.%.3d.%.4d\n", phone_nr_part_1, phone_nr_part_2, \
        phone_nr_part_3);

    return 0;
}
